You have been handed the included image files.  UI. is the application your team is building,  your assignment is to build the views & view controllers that are outlined green area in task.png.

You will find the assets you need in the assets folder.  All other graphics should be generated in code.     

This View Controller is part of an app that helps people to choose what they want to eat from a set of six prompts.  There should be a way (either as a property or a method) to read the choices made by the user.  


###Custom UIControl

A set of six custom UIControls (or one Custom UIControl subclass instantiated six times) that when pressed cycle between a limited number of choices.  

For each control the number of choices is demonstrated by the number of arcs around the circle.  They function the same way as segmented controls (consider its API). The lighter purple color changes position as the user cycles through the options.  

Starting from the upper left and moving across each control has the following choices that it will cycle between:

1. one of a kind / small batch / large batch / mass market

2. savory / sweet / umami

3. spicy / mild

4. crunchy / mushy / smooth

5. a little / a lot

6. breakfast / brunch / lunch / snack / dinner


###Data source
The data for custom UIControls should not be hard coded. Please implement your own approach to configure the custom UIControls.


###Bonus points
1. When the user taps, animate the color/position of the outer arcs.


The goal here is to create the View Controller to be as functional as possible, and as Pixel perfect as possible with code that is self documenting, and easily understandable by another developer.  

Please send a zipped copy of your completed Xcode project to our recruitment team (or via your agent).  Please do not upload the assignment or the solution to any publicly accessible repositories.

If you have any questions feel free to reach out to recruitment@thetrainline.com