//
//  CollectionViewCell.m
//  iOSCodeTest
//
//  Created by Dawid on 20/07/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat alpha = 0.7f;
        self.circleView = [[DPCircleButton alloc] initWithFrame:self.bounds];
        [self.circleView setBackgroundColor:[UIColor colorWithRed:0.46f green:0.46f blue:0.46f alpha:alpha]];
        [self.circleView setNormalSegmentColor:[UIColor colorWithRed:94.0f/255.0f green:9.0f/255.0f blue:40.0f/255.0f alpha:alpha]];
        [self.circleView setSelectedSegmentColor:[UIColor colorWithRed:228.0f/255.0f green:107.0f/255.0f blue:124.0f/255.0f alpha:alpha]];
        [self addSubview:self.circleView];
    }
    return self;
}


@end
