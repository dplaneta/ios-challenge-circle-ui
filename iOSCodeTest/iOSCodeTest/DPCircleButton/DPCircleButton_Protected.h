//
//  DPCircleView_Protected.h
//  iOSCodeTest
//
//  Created by Dawid on 20/07/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "DPCircleButton.h"

@interface DPShapeLayer : CAShapeLayer
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) CGPoint arcCenter;
@property (nonatomic, assign) CGFloat startAngle;
@property (nonatomic, assign) CGFloat endAngle;
@property (nonatomic, assign) CGFloat radius;
@property (nonatomic, assign) CGFloat selectedRadius;
- (void)updatePathForSelected:(BOOL)selected;
@end


@interface DPCircleButton ()
@property (nonatomic, assign) NSUInteger shapeLayerIndex;
@property (nonatomic, strong) NSArray *shapeLayers;
@property (nonatomic, strong) UILabel *label;
- (void)setCircleBackgroundColor:(UIColor *)backgroundColor;
- (void)updateSelectedSegmentIndex:(NSUInteger)index andSendAction:(BOOL)sendAction;
- (DPShapeLayer*)createShapeLayer;
@end
