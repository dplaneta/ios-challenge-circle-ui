//
//  DPCircleView.m
//
//  Created by Dawid on 20/07/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <math.h>
#import <QuartzCore/CALayer.h>
#import "DPCircleButton.h"
#import "DPCircleButton_Protected.h"

#define DEGREES_TO_RADIANS(degrees) ( (M_PI * (degrees)) / 180.0 )
#define RADIANS_TO_DEGREES(radians) ( (180.0 * (radians)) / M_PI )

static CGFloat insideChordMargin = 6.0f;
static CGFloat sRadiusMargin     = 8.0f;
static CGFloat jointPadding      = 4.0f;

@implementation DPCircleButton{
    CAShapeLayer *pCircleBackground;
    UIColor *pBackgroundColor;
}
@synthesize normalSegmentColor = _normalSegmentColor;
@synthesize selectedSegmentColor = _selectedSegmentColor;

- (instancetype)init
{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}



#pragma mark - Setup

- (void)setup
{
    [self setupLayers];
    [self setupLabel];
    [self setupColours];
    [self setNeedsUpdateConstraints];
}

- (void)setupLayers
{
    pCircleBackground = [CAShapeLayer layer];
    [pCircleBackground setLineWidth:1.0f];
    [self.layer addSublayer:pCircleBackground];
}

- (void)setupLabel
{
    self.label = [UILabel new];
    [self.label setTextAlignment:NSTextAlignmentCenter];
    [self.label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self addSubview:self.label];
}

- (void)setupColours
{
    _normalSegmentColor   = [UIColor redColor];
    _selectedSegmentColor = [UIColor greenColor];
    [self setBackgroundColor:[UIColor redColor]];
    [self.label setTextColor:[UIColor whiteColor]];
}


#pragma mark - UIView Override

- (void)updateConstraints
{
    [super updateConstraints];
    
    NSLayoutConstraint *width =[NSLayoutConstraint constraintWithItem:_label attribute:NSLayoutAttributeWidth relatedBy:0 toItem:self attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0];
    NSLayoutConstraint *height =[NSLayoutConstraint constraintWithItem:_label attribute:NSLayoutAttributeHeight relatedBy:0 toItem:self attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:_label attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.f];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:_label attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0f constant:0.f];
    
    [self addConstraints:@[width, height, top, leading]];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect  bounds       = CGRectIntegral(self.bounds);
    CGFloat chord        = roundf(MIN(bounds.size.width, bounds.size.height) - sRadiusMargin*2);
    CGFloat radius       = roundf(0.5f*chord);    
    CGFloat insideChord  = roundf(chord - 2.0f*insideChordMargin);
    CGFloat insideRadius = roundf(0.5f*insideChord);
    
    // Update inside circle.
    CGFloat shift = sRadiusMargin + insideChordMargin;
    [pCircleBackground setPath:[UIBezierPath bezierPathWithRoundedRect:CGRectMake(shift, shift, insideChord, insideChord) cornerRadius:insideRadius].CGPath];
    
    // Update segments.
    CGFloat arc       = [self.shapeLayers count]?360.0f/[self.shapeLayers count]:360.0f;
    CGPoint arcCenter = CGPointMake(chord/2+sRadiusMargin, chord/2+sRadiusMargin);
    [self.shapeLayers enumerateObjectsUsingBlock:^(DPShapeLayer *shapeLayerObject, NSUInteger idx, BOOL *stop) {
        shapeLayerObject.startAngle     = idx*arc + jointPadding;
        shapeLayerObject.endAngle       = shapeLayerObject.startAngle+arc - 2.0f*jointPadding;
        shapeLayerObject.radius         = radius;
        shapeLayerObject.selectedRadius = radius+sRadiusMargin;
        shapeLayerObject.arcCenter      = arcCenter;
        [shapeLayerObject updatePathForSelected:NO];
    }];
}


#pragma mark - UIControl Override

-(BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    [self updateShapesForSelected:YES];
    return [super beginTrackingWithTouch:touch withEvent:event];
}

-(BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGPoint lastPoint     = [touch locationInView:self];
    CGPoint centerPoint   = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    CGFloat currentAngle  = AngleFromNorth(centerPoint, lastPoint, NO);
    CGFloat arcForSection = ([self.shapeLayers count]?360.0/[self.shapeLayers count]: 360.0);
    
    [self updateSelectedSegmentIndex:floor(currentAngle/arcForSection) andSendAction:YES];
    return [super continueTrackingWithTouch:touch withEvent:event];
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    [self updateShapesForSelected:NO];
    [super endTrackingWithTouch:touch withEvent:event];
}


#pragma mark - Setters / Getters

- (void)setItems:(NSArray *)items
{
    // Remove all layers.
    [self.shapeLayers enumerateObjectsUsingBlock:^(DPShapeLayer *obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperlayer];
    }];
    
    // Add layers.
    NSMutableArray *shapes = [[NSMutableArray alloc] initWithCapacity:[items count]];
    [items enumerateObjectsUsingBlock:^(NSString *name, NSUInteger idx, BOOL *stop) {
        CAShapeLayer *layer = [self createShapeLayer];
        NSAssert(layer, @"CAShapeLayer can't be nil!");
        if(0==idx){
            [layer setStrokeColor:_selectedSegmentColor.CGColor];
            [self.label setText:name];
        }
        
        [layer setName:name];
        [shapes addObject:layer];
        [self.layer addSublayer:layer];
    }];
    
    self.shapeLayers = [shapes copy];
    [self layoutIfNeeded];
}

- (NSArray*)items
{
    NSMutableArray *items = [[NSMutableArray alloc] initWithCapacity:[self.shapeLayers count]];
    [self.shapeLayers enumerateObjectsUsingBlock:^(DPShapeLayer *obj, NSUInteger idx, BOOL *stop) {
        [items addObject:obj.name];
    }];
    return [items copy];
}

- (void)setSelectedSegmentIndex:(NSUInteger)index
{
    [self updateSelectedSegmentIndex:index andSendAction:NO];
}


- (NSUInteger)selectedSegmentIndex
{
    return self.shapeLayerIndex;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    pBackgroundColor = backgroundColor;
    [self setCircleBackgroundColor:backgroundColor];
}

- (UIColor*)backgroundColor
{
    return pBackgroundColor;
}

- (void)setSelectedSegmentColor:(UIColor *)selectedSegmentColor
{
    _selectedSegmentColor = selectedSegmentColor;
    [self.shapeLayers[self.shapeLayerIndex] setStrokeColor:_selectedSegmentColor.CGColor];
}

- (void)setFont:(UIFont *)font
{
    [self.label setFont:font];
}

- (UIFont*)font
{
    return [self.label font];
}

- (void)setTextColor:(UIColor *)textColor
{
    [self.label setTextColor:textColor];
}

- (UIColor*)textColor
{
    return [self.label textColor];
}


#pragma mark - Protected API

- (void)setCircleBackgroundColor:(UIColor *)backgroundColor
{
    CGFloat h, s, b, a;
    if ([backgroundColor getHue:&h saturation:&s brightness:&b alpha:&a]){
        pCircleBackground.strokeColor = [UIColor colorWithHue:h saturation:s brightness:b * 0.75 alpha:a].CGColor;
    }
    pCircleBackground.fillColor = backgroundColor.CGColor;
}

- (void)updateSelectedSegmentIndex:(NSUInteger)index andSendAction:(BOOL)sendAction
{
    if(_shapeLayerIndex != index){
        NSAssert(index<[_shapeLayers count], @"Do not exceed the range of segments!");
        
        DPShapeLayer *lastSegment    = self.shapeLayers[self.shapeLayerIndex];
        DPShapeLayer *currentSegment = self.shapeLayers[index];
        [lastSegment setStrokeColor:_normalSegmentColor.CGColor];
        [currentSegment setStrokeColor:_selectedSegmentColor.CGColor];
        [self.label setText:currentSegment.name];
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:[CATransaction animationDuration]];
        [animation setType:kCATransitionFade];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.label.layer addAnimation:animation forKey:@"changeTextTransition"];
        
        self.shapeLayerIndex = index;
        if(sendAction){
            [self sendActionsForControlEvents:UIControlEventValueChanged];
        }
    }
}

- (DPShapeLayer*)createShapeLayer
{
    DPShapeLayer *layer = [DPShapeLayer new];
    [layer setFillColor:[UIColor clearColor].CGColor];
    [layer setStrokeColor:_normalSegmentColor.CGColor];
    [layer setLineWidth:2.0f];
    return layer;
}


#pragma mark - Private API

- (void)updateShapesForSelected:(BOOL)selected
{
    [self.shapeLayers enumerateObjectsUsingBlock:^(DPShapeLayer *shapeLayer, NSUInteger idx, BOOL *stop) {
        [shapeLayer updatePathForSelected:selected];
    }];
    [self setCircleBackgroundColor:selected?_selectedSegmentColor:self.backgroundColor];
}

// Calculate the direction in degrees from a center point to an arbitrary position.
static inline float AngleFromNorth(CGPoint p1, CGPoint p2, BOOL flipped) {
    CGPoint v = CGPointMake(p2.x-p1.x,p2.y-p1.y);
    float vmag = sqrt(v.x*v.x + v.y*v.y);
    v.x /= vmag;
    v.y /= vmag;
    double radians = atan2(v.y,v.x);
    float result = RADIANS_TO_DEGREES(radians);
    return (result>=0.0f ? result : result+360.0f);
}

@end



// __________________________________________________________________________________

#pragma mark - DPShapeLayer -

@implementation DPShapeLayer
@synthesize name;
@synthesize arcCenter;
@synthesize startAngle;
@synthesize endAngle;
@synthesize radius;
@synthesize selectedRadius;

- (void)updatePathForSelected:(BOOL)selected
{
    CGFloat pRadius = selected?selectedRadius:radius;
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:arcCenter radius:pRadius startAngle:DEGREES_TO_RADIANS(startAngle) endAngle:DEGREES_TO_RADIANS(endAngle) clockwise:YES];
    [self setPath:path.CGPath];
}

#pragma mark - CALayer Override

- (id<CAAction>)actionForKey:(NSString *)event {
    if ([event isEqualToString:@"path"]) {
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:event];
        animation.duration = [CATransaction animationDuration];
        animation.timingFunction = [CATransaction animationTimingFunction];
        return animation;
    }
    return [super actionForKey:event];
}

@end
