//
//  DPCircleView.h
//  iOSCodeTest
//
//  Created by Dawid on 20/07/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DPCircleButton <NSObject>
@property (nonatomic, assign) NSUInteger selectedSegmentIndex;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) UIColor *normalSegmentColor;
@property (nonatomic, strong) UIColor *selectedSegmentColor;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *textColor;
@end

@interface DPCircleButton : UIControl <DPCircleButton>

@end
