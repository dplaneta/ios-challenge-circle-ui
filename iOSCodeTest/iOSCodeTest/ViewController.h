//
//  DPViewController.h
//  iOSCodeTest
//
//  Created by Dawid on 20/07/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ModelController;
@interface ViewController : UICollectionViewController
- (instancetype)initWithModelController:(id<ModelController>)modelController;
@end
