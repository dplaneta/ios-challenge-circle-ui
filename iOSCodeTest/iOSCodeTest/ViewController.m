//
//  DPViewController.m
//  iOSCodeTest
//
//  Created by Dawid on 20/07/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "ViewController.h"
#import "ModelController.h"
#import "CollectionViewCell.h"
#import "DPCircleButton.h"

static NSString * const collectionViewCell = @"collectionViewCell";

@implementation ViewController{
    CGSize itemSize;
    id<ModelController> modelController;
}

- (instancetype)initWithModelController:(id<ModelController>)pModelController
{
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
        modelController = pModelController;
        
        itemSize = CGSizeMake(150.0f, 150.0f);
        [layout setItemSize:itemSize];
        
        [self.collectionView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:collectionViewCell];
        [self.collectionView setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MON_Rectangle-5"]]];
    }
    return self;
}


#pragma mark - UIViewController

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationPortrait;
}


#pragma mark - UICollectionViewDelegate

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(26.0f, 0.0f, 0.0f, 0.0f);
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [modelController numberOfItems];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:collectionViewCell forIndexPath:indexPath];
    [cell.circleView setItems:[modelController itemsForIndex:indexPath.row]];
    [cell.circleView addTarget:self action:@selector(eventValueChanged:) forControlEvents:UIControlEventValueChanged];
    return cell;
}


#pragma mark - Private API -

-(void)eventValueChanged:(DPCircleButton*)sender
{
    NSArray *items = [sender items];
    NSLog(@"options: %@, [%@]", [items description], items[[sender selectedSegmentIndex]]);
}

@end
