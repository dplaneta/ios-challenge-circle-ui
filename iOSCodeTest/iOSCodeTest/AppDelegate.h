//
//  AppDelegate.h
//  iOSCodeTest
//
//  Created by Dawid on 20/07/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@end
