//
//  ModelController.h
//  iOSCodeTest
//
//  Created by Dawid on 20/07/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ModelController <NSObject>
@property (nonatomic, assign, readonly) NSUInteger numberOfItems;
- (NSArray*)itemsForIndex:(NSUInteger)index;
@end


@interface ModelController : NSObject <ModelController>

@end
