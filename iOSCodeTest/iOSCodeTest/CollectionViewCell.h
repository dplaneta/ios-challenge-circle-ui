//
//  CollectionViewCell.h
//  iOSCodeTest
//
//  Created by Dawid on 20/07/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DPCircleButton.h"

@interface CollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) DPCircleButton *circleView;
@end
