//
//  ModelController.m
//  iOSCodeTest
//
//  Created by Dawid on 20/07/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "ModelController.h"

@implementation ModelController{
    NSArray *dataSource;
}

- (instancetype)init
{
    self = [super init];
    if(self){
        dataSource = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"appData" ofType:@"plist"]];
    }
    return self;
}


#pragma mark - Public API

- (NSArray*)itemsForIndex:(NSUInteger)index
{
    return dataSource[index];
}


#pragma mark - Getters

- (NSUInteger)numberOfItems
{
    return [dataSource count];
}

@end
