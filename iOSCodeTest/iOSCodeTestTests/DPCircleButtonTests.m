//
//  DPCircleButton.m
//
//  Created by Dawid on 20/07/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DPCircleButton.h"

@interface DPCircleButtonTests : XCTestCase
@end

@implementation DPCircleButtonTests{
    NSArray *items;
}

- (void)setUp
{
    [super setUp];
    items = @[@"item1", @"item2", @"item3"];
    
}

- (void)testConstructor
{
    XCTAssertNotNil([DPCircleButton new], @"Init is not working!");
}

- (void)testItems
{
    DPCircleButton *circleView = [DPCircleButton new];
    circleView.items = items;
    NSUInteger count1 = [circleView.items count];
    NSUInteger count2 = [items count];
    XCTAssertEqual(count1, count2, @"setItems is not working properly! %i!=%i", count1, count2);
}

- (void)testSelectedSegmentIndex
{
    NSUInteger selectedSegmentIndex = 2;
    DPCircleButton *circleView = [DPCircleButton new];
    circleView.items = items;
    circleView.selectedSegmentIndex = selectedSegmentIndex;
    XCTAssertEqual(selectedSegmentIndex, circleView.selectedSegmentIndex, @"setSelectedSegmentIndex is not working properly! %i!=%i", selectedSegmentIndex, circleView.selectedSegmentIndex);
}

@end
